package com.kafka.consumer.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaConsumerProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaConsumerProductApplication.class, args);
	}

}
