package com.kafka.consumer.product.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Service
public interface KafkaProductService {

	void saveConsumerRecordInToDatabase(ConsumerRecord<Integer, String> consumerRecord) throws JsonMappingException, JsonProcessingException;

	void handleRecovery(ConsumerRecord<Integer, String> consumerRecord);

}
