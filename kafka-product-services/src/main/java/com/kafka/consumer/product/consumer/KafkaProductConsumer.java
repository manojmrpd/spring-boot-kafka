package com.kafka.consumer.product.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.kafka.consumer.product.service.KafkaProductService;

import lombok.extern.slf4j.Slf4j;

@EnableKafka
@Component
@Slf4j
public class KafkaProductConsumer {
	
	@Autowired
	KafkaProductService productService;

	@KafkaListener(topics = "inventory")
	public void onMessage(ConsumerRecord<Integer, String> consumerRecord) throws JsonMappingException, JsonProcessingException {
		log.info("Message Received is {} "+consumerRecord.toString());
		productService.saveConsumerRecordInToDatabase(consumerRecord);
	}
	

}
