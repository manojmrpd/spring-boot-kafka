package com.kafka.consumer.product.entity;

public enum InventoryStatus {
	OUT_OF_STOCK,
	AVAILABLE,
	LOW,
	HIGH
}
