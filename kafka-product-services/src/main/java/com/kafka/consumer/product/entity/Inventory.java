package com.kafka.consumer.product.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Inventory {

	@Id
	private Integer productId;

	private Integer availableStock;

	@Enumerated(EnumType.STRING)
	private InventoryStatus inventoryStatus;

	@OneToOne(mappedBy="inventory", cascade=CascadeType.ALL)
	private Product product;

}
