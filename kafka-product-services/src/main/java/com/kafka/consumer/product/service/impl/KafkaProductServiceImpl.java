package com.kafka.consumer.product.service.impl;

import java.util.Optional;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kafka.consumer.product.entity.Product;
import com.kafka.consumer.product.repository.ProductRepository;
import com.kafka.consumer.product.service.KafkaProductService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class KafkaProductServiceImpl implements KafkaProductService {
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	KafkaTemplate<Integer, String> kafkaTemplate;
	

	@Override
	public void saveConsumerRecordInToDatabase(ConsumerRecord<Integer, String> consumerRecord) throws JsonMappingException, JsonProcessingException {
		
		Product product = objectMapper.readValue(consumerRecord.value(), Product.class);
		
		if(product.getProductId() ==null && product.getProductId() == 000) {
			throw new RecoverableDataAccessException("Temporary Network Issue");
		}
		
		switch(product.getProductType()){
        case NEW:
            save(product);
            break;
        case UPDATE:
            validate(product);
            save(product);
            break;
        default:
            log.info("Invalid Product Type");
    }
		
	}
	
	private void validate(Product product) {
		if(product.getProductId() == null) {
			throw new IllegalArgumentException("Product Id is missing");
		}
		
		Optional<Product> productDetail = productRepository.findById(product.getProductId());
		if(!productDetail.isPresent()) {
			throw new IllegalArgumentException("Not a valid product");
		}
	}

	private void save(Product product) {
		product.getInventory().setProduct(product);
        productRepository.save(product);
        log.info("Successfully Persisted the products with new inventory {} ", product);
    }

	@Override
	public void handleRecovery(ConsumerRecord<Integer,String> record){

        Integer key = record.key();
        String value = record.value();

        ListenableFuture<SendResult<Integer,String>> listenableFuture = kafkaTemplate.sendDefault(key, value);
        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {

			@Override
			public void onSuccess(SendResult<Integer, String> result) {
				handleSuccess(key, value, result);
				
			}

			@Override
			public void onFailure(Throwable ex) {
				handleFailure(key, value, ex);
			}
		});
	}
	
	private void handleFailure(Integer key, String value, Throwable ex) {
        log.error("Error Sending the Message and the exception is {}", ex.getMessage());
        try {
            throw ex;
        } catch (Throwable throwable) {
            log.error("Error in OnFailure: {}", throwable.getMessage());
        }
    }

    private void handleSuccess(Integer key, String value, SendResult<Integer, String> result) {
        log.info("Message Sent SuccessFully for the key : {} and the value is {} , partition is {}", key, value, result.getRecordMetadata().partition());
    }

}
