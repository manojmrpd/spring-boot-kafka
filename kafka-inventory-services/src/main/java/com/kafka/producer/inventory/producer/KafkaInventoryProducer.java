package com.kafka.producer.inventory.producer;

import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kafka.producer.inventory.model.Product;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class KafkaInventoryProducer {

	@Autowired
	KafkaTemplate<Integer, String> kafkaTemplate;

	@Autowired
	ObjectMapper objectMapper;

	private String topic = "inventory";

	public void sendItemAvaiabilityAsync(Product product) throws JsonProcessingException {

		Integer key = product.getProductId();
		String value = objectMapper.writeValueAsString(product);
		ProducerRecord<Integer, String> record = getProductRecord(key, value);
		ListenableFuture<SendResult<Integer, String>> listenableFuture = kafkaTemplate.send(record);
		listenableFuture.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {

			@Override
			public void onSuccess(SendResult<Integer, String> result) {
				handleSuccess(key, value, result);

			}

			@Override
			public void onFailure(Throwable ex) {
				handleFailure(key, value, ex);

			}
		});

	}

	private ProducerRecord<Integer, String> getProductRecord(Integer key, String value) {
		return new ProducerRecord<Integer, String>(topic, key, value);
	}

	private void handleFailure(Integer key, String value, Throwable ex) {
		log.error("Error Sending the Message and the exception is {}", ex.getMessage());
		try {
			throw ex;
		} catch (Throwable throwable) {
			log.error("Error in OnFailure: {}", throwable.getMessage());
		}

	}

	private void handleSuccess(Integer key, String value, SendResult<Integer, String> result) {
		log.info("Message Sent SuccessFully for the key : {} and the value is {} , partition is {}", key, value,
				result.getRecordMetadata().partition());
	}

	public SendResult<Integer, String>  sendItemAvaiabilitySync(Product product) throws JsonProcessingException {
		
		SendResult<Integer, String> sendResult = null;
		Integer key = product.getProductId();
		String value = objectMapper.writeValueAsString(product);
		ProducerRecord<Integer, String> record = getProductRecord(key, value);
		try {
			sendResult = kafkaTemplate.send(record).get();
		} catch (InterruptedException | ExecutionException e) {
			log.error("Error occured while sending the message:: "+e.getMessage());
		}
		
		return sendResult;
	}

}
