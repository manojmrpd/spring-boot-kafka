package com.kafka.producer.inventory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kafka.producer.inventory.model.Product;
import com.kafka.producer.inventory.model.ProductType;
import com.kafka.producer.inventory.producer.KafkaInventoryProducer;

@RestController
@RequestMapping("v1")
public class KafkaInventoryController {
	
	
	@Autowired
	private KafkaInventoryProducer producer;
	
	
	@PostMapping("/inventory")
	public ResponseEntity<Product> add(@RequestBody Product product) throws JsonProcessingException {
		product.setProductType(ProductType.NEW);
		producer.sendItemAvaiabilityAsync(product);
		//producer.sendItemAvaiabilitySync(product);
		return new ResponseEntity<Product>(product, HttpStatus.CREATED);
	}
	
	@PutMapping("/inventory")
	public ResponseEntity<Product> update(@RequestBody Product product) throws JsonProcessingException {
		product.setProductType(ProductType.UPDATE);
		producer.sendItemAvaiabilityAsync(product);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

}
