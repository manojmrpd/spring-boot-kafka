package com.kafka.producer.inventory.model;

public enum InventoryStatus {
	OUT_OF_STOCK,
	AVAILABLE,
	LOW,
	HIGH
}
